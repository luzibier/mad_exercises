from builtins import print

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import random as rd


def least_square(A, b):
    A_T = np.transpose(A)
    inv = np.linalg.inv(np.dot(A_T, A))
    ATb = np.dot(A_T, b)
    return np.dot(inv, ATb)


N = 100
plane_length = 1.0
G = 5
plane_size = int (N**.5)

"""Generating plane just for good measure"""
plane = np.ones((plane_size, plane_size))
for i in range(plane_size):
    for j in range(plane_size):
        plane[i][j] = 1.0 * plane_length / plane_size * i + 1.0 * plane_length / plane_size * j + G*rd.uniform(-0.1, 0.1)


b = np.ones(plane_size**2)
A = np.ones((plane_size**2, 3))
t = np.ones(plane_size)
for i in range(plane_size):
    t[i] = 1.0 * plane_length / plane_size * i
    for j in range(plane_size):
        coord = i * plane_size + j
        A[coord][1] = 1.0 * plane_length / plane_size * i
        A[coord][2] = 1.0 * plane_length / plane_size * j
        b[coord] = plane[i][j]


x = least_square(A, b)

set_size = 10
A2 = np.ones((set_size, 3))
b2 = np.ones(set_size)
for i in range(set_size):
    A2[i][1] = rd.uniform(0, 1.0)
    A2[i][2] = rd.uniform(0, 1.0)
    b[i] = A2[i][2] + A2[i][1] + G * rd.uniform(-0.1, 0.1)
 
x2 = least_square(A2, b2)

print("Part 2: alpha = ", x[0], " beta = ", x[1], " gamma = ", x[2])
print("Part 3: alpha = ", x2[0], " beta = ", x2[1], " gamma = ", x2[2])


fig = plt.figure(0)
ax = plt.axes(projection='3d')


ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')

tmp = np.outer(np.ones(plane_size), t)
tmp_T = tmp.copy().T

ax.plot_surface(tmp, tmp_T, plane, label='generated Data')
ax.plot_surface(tmp, tmp_T, x[1]*tmp + x[2]*tmp_T + x[0], label='Least square 3D')

plt.show()
