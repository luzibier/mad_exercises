from builtins import print

import matplotlib.pyplot as plt
import numpy as np
import random as rd


def least_square(x, b):
    A = np.ones((len(x), 2))
    for i in range(len(x)):
        A[i][0] = x[i]

    A_T = np.transpose(A)
    inv = np.linalg.inv(np.dot(A_T, A))
    ATb = np.dot(A_T, b)
    return np.dot(inv, ATb)


current = np.array([0.01, 0.02, 0.03, 0.04,  0.05,  0.06,  0.07,  0.08,  0.09,  0.10]);
voltage = np.array([3.92, 6.23, 5.52, 8.45, 10.11, 15.11, 15.11, 18.22, 19.97, 12.77]);
t = np.linspace(0, 20, 200)
G = 1.0  # Noise Level


#uncomment for part 01
# x = (R, V_0)^T
x = least_square(voltage, current)

gen_volt = 150 * current - 0.5
gen_volt_noise = np.copy(gen_volt)
for i in range(len(gen_volt_noise)):
    gen_volt_noise[i] += G*rd.uniform(-1.0, 1.0)

gen_volt[7] = 0.5*gen_volt[7]

x_gen = least_square(gen_volt, current)
x_gen_noise = least_square(gen_volt_noise, current)

ax = plt.figure(0)
plt.xlabel('Voltage [V]')
plt.ylabel('Current [A]')
# plt.plot(voltage, current, '*r')
# plt.plot(t, t*x[0]+x[1], '-r')

plt.plot(gen_volt, current, 'b*', label='generated one outlier')
plt.plot(t, t*x_gen[0]+x_gen[1], '-b')
plt.plot(gen_volt_noise, current, 'g*', label='generated noise')
plt.plot(t, t*x_gen_noise[0]+x_gen_noise[1], '-g')
ax.legend()


plt.show()

