import numpy as np


x0 = np.array([2, 3, 4])    # initial guesses of k
tol = 10**(-5)              # Tolerance
i_max = 20                  # maximum of iterations

load = 50000                # load expected to be lifted from the final pillar
est_r_0 = 4                 # first estimation of the radius of the pillar for secant method
est_r_1 = 2                 # second estimation of the radius of the pillar for secant method

b = np.array((100, 120, 150))
r = np.array((.1, .2, .3))


i = 0
x = x0


def f(k):
    return k[0]*np.exp(k[1] * r) + k[2] * r - b


def df_dk1(k):
    return np.exp(k[1] * r)


def df_dk2(k):
    return k[0] * r * np.exp(k[1] * r)


def df_dk3(k):
    return r


for i in range(i_max):
    J = np.array([df_dk1(x), df_dk2(x), df_dk3(x)]).T   # Jacobian
    y = np.linalg.solve(J, -f(x))                       # Solve for y
    x = x + y                                           # updating
    if np.linalg.norm(y) < tol:
        print("k = ", x)
        break


if i+1 == i_max:
    print("Reached max nr of iterations, try adjusting x0")
else:
    # Using Secant method to find the corresponding r
    f = lambda r: r**2 * np.pi*(x[0]*np.exp(x[1]*r)+x[2])-load
    while f(max(est_r_1, est_r_0)) > tol:
        r = est_r_1 - f(est_r_1)*(est_r_1-est_r_0)/(f(est_r_1)-f(est_r_0))
        est_r_0 = est_r_1
        est_r_1 = r
    print("r needs to be at least of the size of ", max(est_r_1, est_r_0))
