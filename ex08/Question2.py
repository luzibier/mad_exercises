import numpy as np
from math import sqrt, pi, exp
from scipy import linalg


def trapezoidal_integration(x, f_x):
    res = 0
    for i in range(len(x)-1):
        res += (f_x[i] + f_x[i+1])/2*(x[i+1]-x[i])
    return res


if __name__ == "__main__":
    rad = np.loadtxt("data.txt", dtype=float, usecols=0)
    temp = np.loadtxt("data.txt", dtype=float, usecols=1)
    theta = 0.7051

    avg_temp = trapezoidal_integration(rad, np.multiply(rad, temp)*theta)/trapezoidal_integration(rad, rad*theta)

    print(avg_temp)
