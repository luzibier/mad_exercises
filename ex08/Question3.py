import numpy as np
from math import sqrt, pi, exp
from scipy import linalg


def exact_value(r_0, r_e, theta, a, b, c):
    temp = theta*(a/(b+2.0)*(r_0**(b+2.0)-r_e**(b+2.0)) + c/2.0*(r_0**2-r_e**2))
    area = theta/2.0*(r_0**2-r_e**2)
    return temp/area


def trapezoidal_integration(x, f_x):
    res = 0
    for i in range(len(x)-1):
        res += (f_x[i] + f_x[i+1])/2*(x[i+1]-x[i])
    return res


def simpson_integration(x, f_x):
    res = 0
    for i in range(len(x)-2):
        res += (f_x[i] + 4*f_x[i+1] + f_x[i+2])/6*(x[i+2]-x[i])
    return res


if __name__ == "__main__":
    rad = np.loadtxt("data.txt", dtype=float, usecols=0)
    temp = np.loadtxt("data.txt", dtype=float, usecols=1)
    theta = 0.7051

    trapez = trapezoidal_integration(rad, np.multiply(rad, temp)*theta)/trapezoidal_integration(rad, rad*theta)
    exact = exact_value(rad[-1], rad[0], theta, -.125, -3.455, 770.3)
    simpson = simpson_integration(rad, np.multiply(rad, temp)*theta)/simpson_integration(rad, rad*theta)

    print("Exact value ", exact)
    print("Trapez   ", trapez, "       Absolute error: ", abs(trapez-exact), "    Relative error: ", abs((trapez-exact)/exact))
    print("Simpson  ", simpson, "       Absolute error: ", abs(simpson-exact), "    Relative error: ", abs((simpson-exact)/exact))
