import numpy as np
import matplotlib.pyplot as plt


# function and its first and second derivative
prec = 10**(-2)
f = lambda x: x**2-34
df_dx = lambda x: 2*x
d2f_dx2 = lambda x: 2
x_0 = 6.0
error_0 = 30

t = np.linspace(5.8, 6.1, 300)
plt.plot(t, f(t), '-b')


def next_newton(x, func, der):
    return x - func(x)/der(x)


def next_error(err, der1, der2):
    return abs(der2(x)/(2.0*der1(x)))*err**2


x = x_0
error = error_0

print("Initial Solution: ", x, " Error", error)
plt.plot(x, 0, '*r')

while error > prec:
    x = next_newton(x, f, df_dx)
    plt.plot(x, 0, '*r')
    error = next_error(error, df_dx, d2f_dx2)
    print("Solution: ", x, " Error", error)

plt.show()


