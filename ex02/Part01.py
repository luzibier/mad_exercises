import numpy as np

M = 15  # from the Question
N = 50  # from the Question

A = np.ones((N, M))
x = np.linspace(1, 15, 15)
b = np.dot(A, x)

for i in range(N):
    for j in range(M):
        A[i][j] = (i / 49) ** j


# Pseudo inverse
A_inv = np.linalg.pinv(A)
k = np.linalg.norm(A, 2)*np.linalg.norm(A_inv, 2)

# x = (A^T*A)^-1*b
x_bar_norm = np.dot(np.dot(np.linalg.pinv(np.dot(A.T, A)), A.T), b)
# residual p(x)= ||Ax - b||2/( ||A||2 * ||b||2)
res_norm = np.linalg.norm(np.dot(A, x_bar_norm) - b)
# error = ||x - x_bar_norm_norm||2
err_norm = np.linalg.norm(x - x_bar_norm, 2)


u, s, v = np.linalg.svd(A)
# casting s to the correct dimensions
sigma_plus = np.zeros_like(A)
s = np.diag(s)
sigma_plus[:s.shape[0], :s.shape[1]] = s

# xbar
x_bar_svd = np.dot(np.dot(np.dot(v, np.linalg.pinv(sigma_plus)), u.T), b)
# residual
res_svd = np.linalg.norm(np.dot(A, x_bar_svd) - b)
# error
err_svd = np.linalg.norm(x - x_bar_svd, 2)

print("Condition number equals to: ", k)
print()
print("residual of normal equation equals to: ", res_norm)
print("Error of normal equation equals to: ", err_norm)
print()
print("residual of svd equals to: ", res_svd)
print("Error of svd equals to: ", err_svd)

