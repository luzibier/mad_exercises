import numpy as np
from matplotlib import pyplot as plt


def f(x):
    """
    Function to be interpolated by lagrange
    :param x: x
    :return: function of x
    """
    return np.power(1+np.power(x, 2), -.5)


N = 10
x_data = np.linspace(-5, 5, N+1)
t = np.linspace(-5, 5, 201)
y_data = f(x_data)


# smart people would safe the coefficients...
def calc_lagrange_interpolant(x, x_values, i):
    """
    calculates the i-th lagrange interpolant
    smart poeple would save
    :param x: function value
    :param x_values: list
    :param i: iterator which interpolant is searched
    :return: i-th lagrange interpolant
    """
    value = 1
    x_i = x_values[i]
    for j in range(len(x_values)):
        if j == i:
            continue
        x_m = x_values[j]
        value *= (x-x_m)/(x_i-x_m)
    return value


# and not recalculating it every time
def calc_lagrange(x, x_values, y_values):
    """
    gives back the lagrange interpolation of a specific point
    :param x: point x
    :param x_values: x_data values
    :param y_values: y_data values
    :return: lagrange interpolation of x
    """
    assert(len(x_values) == len(y_values))
    total_l = 0
    for i in range(len(x_values)):
        total_l += y_values[i]*calc_lagrange_interpolant(x, x_values, i)
    return total_l


print("Calculating Lagrange")
t_calc = np.empty_like(t)
for i in range(len(t)):
    print("Calculating No. ", i)
    t_calc[i] = calc_lagrange(t[i], x_data, y_data)


# plotting
ax = plt.figure(0)
plt.xlabel('X')
plt.ylabel('Y')

plt.plot(x_data, y_data, 'b*', label='Sample Points')
plt.plot(t, f(t), '-r', label='True function')
plt.plot(t, t_calc, '-g', label='Laplace interpolation')
plt.grid(True)
ax.legend()


plt.show()



