import numpy as np
from matplotlib import pyplot as plt

# data points with x, y, weight
data = np.array([[-1.0,  0.0, 1.0],
                [ -1.0,  1.0, 2**.5/2],
                [  0.0,  1.0, 1.0],
                [  1.0,  1.0, 2**.5/2],
                [  1.0,  0.0, 1.0],
                [  1.0, -1.0, 2**.5/2],
                [  0.0, -1.0, 1.0],
                [ -1.0, -1.0, 2**.5/2],
                [ -1.0,  0.0, 1.0]])


t = [0.0, 0.0, 0.0, .25, .25, .5, .5, .75, .75, 1.0, 1.0, 1.0]
precision = 500
d = 2


s = np.linspace(t[d], t[len(data)], precision+1)
coords = np.zeros((len(s), 2))


def calcB(i, d, t, x):
    if d == 0:
        if t[i] <= x < t[i+1]:
            return 1
        else:
            return 0
    else:
        valA = 0
        valB = 0
        if t[i+d]-t[i] != 0:
            valA = (x-t[i])/(t[i+d]-t[i])*calcB(i, d-1, t, x)
        if t[i+d+1]-t[i+1] != 0:
            valB = (t[i+d+1]-x)/(t[i+d+1]-t[i+1])*calcB(i+1, d-1, t, x)
        return valA + valB


def calcR(i, d, t, w, s):
    denominator = 0
    for j in range(len(w)):
        denominator += w[j]*calcB(j, d, t, s)

    if denominator == 0:
        return 0
    else:
        return calcB(i, d, t, s)*w[i]/denominator


for s_point in range(len(s)):
    for point in range(len(data)):
        r = calcR(point, d, t, data[:, 2], s[s_point])
        coords[s_point][0] += r * data[point][0]
        coords[s_point][1] += r * data[point][1]


ax = plt.figure(0)
plt.xlabel('X')
plt.ylabel('Y')
plt.plot(coords[1:-1, 0], coords[1:-1, 1], 'b-', label="Spline")
plt.plot(data[:, 0], data[:, 1], 'r*', label="Data")
plt.grid(True)
ax.legend()

plt.show()