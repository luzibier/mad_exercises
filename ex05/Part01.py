import numpy as np
from matplotlib import pyplot as plt

i = 1
d = 2
t = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
x = np.linspace(-.2, 1.2, 500)
y = np.zeros_like(x)


# Adjusted to start calculating at 0
i = i - 1

def calcB(i, d, t, x):
    if d == 0:
        if t[i] <= x < t[i+1]:
            return 1
        else:
            return 0
    else:
        valA = 0
        valB = 0
        if t[i+d]-t[i] != 0:
            valA = (x-t[i])/(t[i+d]-t[i])*calcB(i, d-1, t, x)
        if t[i+d+1]-t[i+1] != 0:
            valB = (t[i+d+1]-x)/(t[i+d+1]-t[i+1])*calcB(i+1, d-1, t, x)
        return valA + valB


for j in range(len(x)):
    y[j] = calcB(i, d, t, x[j])


ax = plt.figure(0)
plt.xlabel('X')
plt.ylabel('Y')

plt.plot(x, y, 'b-', label="Spline")
plt.grid(True)
ax.legend()

plt.show()